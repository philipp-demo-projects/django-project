
help:
	@echo "  clean                       clean files"
	@echo "  test                        run the testsuite"
	@echo "  lint                        check the source for style errors"
	@echo "  format                      format the python code"
	@echo "  run-local                   run the server in localhost with debug and autoreload (development mode)"
	@echo "  run                         run the server in localhost (production mode)"

.PHONY: clean
clean:
	@echo "--> Cleaning pyc files"
	find . -name "*.pyc" -delete

.PHONY: test
test:
	@echo "--> Running unittest"
	pytest --verbose --cov=src --cov=tests --cov-report=term-missing

.PHONY: lint
lint:
	@echo "--> Analyse code"
	flake8 src/ tests/
	@echo "--> flake8 done"
	mypy src/

.PHONY: format
format:
	@echo "--> Format the python code"
	autoflake --remove-all-unused-imports --remove-unused-variables  --recursive --in-place src/ tests/
	black --line-length 100 src tests
	isort src tests

.PHONY: run-dev
run-dev:
	@echo "--> Running dev server"
	python src/main.py

.PHONY: run
run:
	@echo "--> Running server"
	python src/main.py
